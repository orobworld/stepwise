This repository contains R code to implement stepwise linear regression using F tests (equivalently, coefficient p-values) as is often taught in introductory regression texts. Users need only download either the `demo.Rmd` file (an R notebook containing the stepwise function and demonstration code) or the `demo.nb.html` file (the same demonstration in HTML format, including embedded R code which can be extracted via the "Code" select button in the upper right).

This code is freely available under a Creative Commons license.
